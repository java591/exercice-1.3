package fr.cnam.foad.nfa035.fileutils.streaming.media;

public interface ImageFrameMedia {
    void getChannel();
    void getEncodedImageOutput();
}
