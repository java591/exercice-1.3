package fr.cnam.foad.nfa035.fileutils.streaming.media;

public interface ResumableImageFrameMedia {
    void getEncodedImageReader();
}
