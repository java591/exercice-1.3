package fr.cnam.foad.nfa035.fileutils.streaming.test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StreamingTestTest {


    @Test
    void main() {
    }

    @Test
    void difference() {
        String res =StreamingTest.difference("string test","string test test");
        assertTrue(res.equals( " test"));
    }

    @Test
    void indexOfDifference() {
        int res = StreamingTest.indexOfDifference("char sequence test","char sequence test2");
        assertTrue(res==18);
    }
}